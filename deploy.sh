#!/usr/bin/env bash

# This is not meant to be portable (nor make sense) and is likely only useful
# when run in the environment it was specifically written for.


# You need to manually list private files/folders in the array below

private=(  )

DIR=$(cd "$(dirname "$0")" && pwd)

[ -e "${DIR}/temp.tar" ] && echo " ERROR: ${DIR}/temp.tar exists. Either \
something went wrong with this script on a previous run, or there's a temp.tar \
unrelated to this script. Either way, I quit." && exit 1 

echo -n "Decrypt (OpenSSL) and unpack private.tar? (y/n)  "
read; answer=$(awk '{print tolower($0)}' <<<$REPLY)
[ "$answer" == "y" ] && openssl enc -d -aes-256-cbc < "${DIR}/private.tar.aes" \
| tar xvf - || ( echo -n "Archive, encrypt (OpenSSL), and then delete all \
private files/folders (y/n)  "; read; answer=$(awk '{print tolower($0)}' \
<<<$REPLY); [ "$answer" == "y" ] && cd "$DIR" && tar cvf temp.tar \
"${private[@]}" && openssl enc -e -aes-256-cbc < temp.tar > private.tar.aes \
&& rm temp.tar && rm -ri ${private[@]} )

echo -n "Create symlinks as defined in symlinks.sh? (y/n)  "
read; answer=$(awk '{print tolower($0)}' <<<$REPLY)
[ "$answer" == "y" ] && cd ${DIR} && ./symlinks.sh

echo -n "Read notes.aes? (y/n)  "
read; answer=$(awk '{print tolower($0)}' <<<$REPLY)
[ "$answer" == "y" ] && openssl enc -d -aes-256-cbc < notes.aes
