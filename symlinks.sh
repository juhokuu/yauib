#!/usr/bin/env bash

# Deploys the commands listed in the 'links' array from commands-available.
# Array structure:
#    key = command as it is to be called on IRC
#    value = the actual command in 'commands-available'
#
# NOTE:
#   - This script prefixes each 'key' with a '!'.
#   - Arrays with key-value pairs require Bash 4.0 or greater

# Relative path
YPATH="`dirname \"$0\"`"

# Absolute, normalized path
YPATH="`( cd \"$YPATH\" && pwd )`"
if [ -z "$YPATH" ] ; then
	exit 1
fi
echo "Using absolute path $YPATH"

declare -A links
links[bang]="bangbang"
links[batsignal]="send_sms.py"
links[btc]="bitcoin"
links[cpu-syöpöt]="cpu-syöpöt"
links[deluge]="deluge"
links[games]="worms"
links[haloo]="haloo"
links[help]="help"
links[homer]="homer"
links[ismo]="ismo"
links[kopukopu]="kopukopu"
links[kovo]="kovo"
links[kuka]="kuka"
links[laskeppa]="laskeppa"
links[ledi]="ledi"
links[mail]="mail"
links[muista]="muista"
links[pennut]="pennut"
links[reitit]="routes.sh"
links[reititin-arp]="reititin-arp-taulu"
links[reititin-reitit]="reititin-reitit"
links[sano]="sano"
links[sää]="weat"
links[uptime]="uptime"
links[verkko]="verkko"
links[viimeiset]="viimeiset"
links[vol]="vol"

for i in "${!links[@]}"; do
	ln -v -s "${YPATH}/commands-available/${links[$i]}" "${YPATH}/commands-enabled/!$i"
done

declare -A alinks
alinks[ggcommand]="ggcommand"
alinks[hopic]="hopic"
alinks[joo]="bitcoin"
alinks[joo,]="bitcoin"
alinks[jöö]="jöö"
alinks[jöö,]="jöö"
alinks[nii]="nii"
alinks[nii,]="nii"

for i in "${!alinks[@]}"; do
	ln -v -s "${YPATH}/advanced-commands-available/${alinks[$i]}" "${YPATH}/advanced-commands-enabled/$i"
done

declare -A hooks
hooks[pubmsg]="command"
hooks[privmsg]="command-priv"
hooks[all_raw_messages]="join"

for i in "${!hooks[@]}"; do
	ln -v -s "${YPATH}/hooks-available/${hooks[$i]}" "${YPATH}/hooks-enabled/$i"
done
